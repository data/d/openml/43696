# OpenML dataset: IBM-HR-Analytics-Employee-Attrition--Performance

https://www.openml.org/d/43696

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

Uncover the factors that lead to employee attrition and explore important questions such as show me a breakdown of distance from home by job role and attrition or compare average monthly income by education and attrition. This is a fictional data set created by IBM data scientists.
Education
    1 'Below College'
    2 'College'
    3 'Bachelor'
    4 'Master'
    5 'Doctor'
EnvironmentSatisfaction
    1 'Low'
    2 'Medium'
    3 'High'
    4 'Very High'
JobInvolvement    
        1 'Low'
    2 'Medium'
    3 'High'
    4 'Very High'
JobSatisfaction    
       1 'Low'
    2 'Medium'
    3 'High'
    4 'Very High'
PerformanceRating    
        1 'Low'
    2 'Good'
    3 'Excellent'
    4 'Outstanding'
RelationshipSatisfaction    
        1 'Low'
    2 'Medium'
    3 'High'
    4 'Very High'
WorkLifeBalance    
        1 'Bad'
    2 'Good'
    3 'Better'
    4 'Best'

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/43696) of an [OpenML dataset](https://www.openml.org/d/43696). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/43696/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/43696/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/43696/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

